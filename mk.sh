#!/bin/bash
#
# Description	: rockchip linux Build Script for x3399.
# Authors	: www.9tripod.com
# Version	: 1.00
# Notes		: support gpt linux
#

#
# Some Directories
#
BS_DIR_TOP=$(cd `dirname $0` ; pwd)
BS_DIR_TOOLS=${BS_DIR_TOP}/tools
BS_DIR_OUTPUT=${BS_DIR_TOP}/output
BS_DIR_UBOOT=${BS_DIR_TOP}/u-boot
BS_DIR_KERNEL=${BS_DIR_TOP}/kernel
BS_DIR_BUILDROOT=${BS_DIR_TOP}/buildroot

#
# CROSS_COMPILE
#
BS_DIR_TOOLCHAIN_ARM64=${BS_DIR_TOP}/prebuilts/gcc/linux-x86/aarch64/gcc-linaro-6.3.1-2017.05-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-

#
# Target Config
#
BS_CONFIG_BOOTLOADER_UBOOT=9tripod-rk3399_defconfig
BS_CONFIG_KERNEL=x3399_linux_defconfig
BS_CONFIG_KERNEL_DTB=x3399-linux.img
BS_CONFIT_ROOTFS=rockchip_rk3399
BS_CONFIT_RECOVERY=rockchip_rk3399_recovery


setup_environment()
{
	LANG=C
	cd ${BS_DIR_TOP};
	mkdir -p ${BS_DIR_OUTPUT} || return 1
	[ -f "${BS_DIR_OUTPUT}/upgrade_tool" ] || { cp -a ${BS_DIR_TOOLS}/linux/Linux_Upgrade_Tool/Linux_Upgrade_Tool/upgrade_tool ${BS_DIR_OUTPUT};}
	[ -f "${BS_DIR_OUTPUT}/config.ini" ] || { cp -a ${BS_DIR_TOOLS}/linux/Linux_Upgrade_Tool/Linux_Upgrade_Tool/config.ini ${BS_DIR_OUTPUT};}
}

build_bootloader_uboot()
{
	if [ -f ${BS_DIR_UBOOT}/*_loader_*.bin ]; then
		rm ${BS_DIR_UBOOT}/*_loader_*.bin
	fi

# Compiler uboot
	cd ${BS_DIR_UBOOT} || return 1
	#make distclean || return 1
	make ${BS_CONFIG_BOOTLOADER_UBOOT} CROSS_COMPILE=${BS_DIR_TOOLCHAIN_ARM64}|| return 1
	make -j${threads} CROSS_COMPILE=${BS_DIR_TOOLCHAIN_ARM64}|| return 1

#pack uboot.img
	echo "--------pack uboot.img--------"
	UBOOT_LOAD_ADDR=`sed -n "/CONFIG_SYS_TEXT_BASE=/s/CONFIG_SYS_TEXT_BASE=//p" ${BS_DIR_UBOOT}/include/autoconf.mk|tr -d '\r'`
	${BS_DIR_TOP}/rkbin/tools/loaderimage --pack --uboot ${BS_DIR_UBOOT}/u-boot.bin uboot.img ${UBOOT_LOAD_ADDR} || return 1
	# Delete u-boot.img and u-boot-dtb.img, which makes users not be confused with final uboot.img
	if [ -f ${BS_DIR_UBOOT}/u-boot.img ]; then
		rm ${BS_DIR_UBOOT}/u-boot.img
	fi
	if [ -f ${BS_DIR_UBOOT}/u-boot-dtb.img ]; then
		rm ${BS_DIR_UBOOT}/u-boot-dtb.img
	fi
	echo "pack uboot okay! Input: ${BS_DIR_UBOOT}/u-boot.bin"

#pack *_loader_*.bin
	echo "--------pack loader--------"
	cd ${BS_DIR_TOP}/rkbin
	${BS_DIR_TOP}/rkbin/tools/boot_merger --replace tools/rk_tools/ ./ ${BS_DIR_TOP}/rkbin/RKBOOT/RK3399MINIALL.ini || return 1
	mv ${BS_DIR_TOP}/rkbin/*_loader_*.bin ${BS_DIR_UBOOT}/ || return 1
	cd -
	echo "pack loader okay! Input: ${BS_DIR_TOP}/rkbin/RKBOOT/RK3399MINIALL.ini"

#pack trust.img
	echo "--------pack trust.img--------"
	cd ${BS_DIR_TOP}/rkbin
	# ARM64 uses trust_merger
	if grep -Eq ''^CONFIG_ARM64=y'|'^CONFIG_ARM64_BOOT_AARCH32=y'' ${BS_DIR_UBOOT}/.config ; then
		${BS_DIR_TOP}/rkbin/tools/trust_merger --replace tools/rk_tools/ ./ ${BS_DIR_TOP}/rkbin/RKTRUST/RK3399TRUST.ini || return 1
		cd -
		mv ${BS_DIR_TOP}/rkbin/trust.img ${BS_DIR_UBOOT} || return 1
		echo "pack trust okay! Input: ${BS_DIR_TOP}/rkbin/RKTRUST/RK3399TRUST.ini"
	fi

# Copy bootloader to release directory
	cp -v ${BS_DIR_UBOOT}/*_loader_*.bin ${BS_DIR_OUTPUT}/MiniLoaderAll.bin || return 1
	cp -v ${BS_DIR_UBOOT}/uboot.img ${BS_DIR_OUTPUT} || return 1
	cp -v ${BS_DIR_UBOOT}/trust.img ${BS_DIR_OUTPUT} || return 1
	return 0
}

build_kernel()
{
	# Compiler kernel
	cd ${BS_DIR_KERNEL} || return 1

	make ARCH=arm64 ${BS_CONFIG_KERNEL} CROSS_COMPILE=${BS_DIR_TOOLCHAIN_ARM64} || return 1
	#make ARCH=arm64 Image CROSS_COMPILE=${BS_DIR_TOOLCHAIN_ARM64} -j${threads} || return 1
	make ARCH=arm64 ${BS_CONFIG_KERNEL_DTB} CROSS_COMPILE=${BS_DIR_TOOLCHAIN_ARM64} -j${threads} || return 1

	# Copy kernel.img & resource.img to release directory
	#cp -v ${BS_DIR_KERNEL}/kernel.img ${BS_DIR_OUTPUT} || return 1
	#cp -v ${BS_DIR_KERNEL}/resource.img ${BS_DIR_OUTPUT} || return 1
	cp -v ${BS_DIR_KERNEL}/boot.img ${BS_DIR_OUTPUT}/boot.img || return 1

	return 0
}

build_buildroot()
{
	# build buildroot
	cd ${BS_DIR_TOP} || return 1
	source ${BS_DIR_BUILDROOT}/build/envsetup.sh $BS_CONFIT_ROOTFS || return 1
	make || return 1
	cp -v ${BS_DIR_BUILDROOT}/output/$BS_CONFIT_ROOTFS/images/rootfs.ext4 ${BS_DIR_OUTPUT}/rootfs-linux.img || return 1
	return 0
}

build_recovery()
{
# build kernel
	KERNEL_IMAGE=${BS_DIR_KERNEL}/arch/arm64/boot/Image
	KERNEL_DTB=${BS_DIR_KERNEL}/resource.img

	if [ -f ${KERNEL_IMAGE} ]
	then
		echo "found kernel image"
	else
		echo "kernel image doesn't exist, now build kernel image"
		build_kernel || return 1
		if [ $? -eq 0 ]; then
			echo "build kernel done"
		else
			exit 1
		fi
	fi

# build recovery
	cd ${BS_DIR_TOP} || return 1
	source ${BS_DIR_BUILDROOT}/build/envsetup.sh ${BS_CONFIT_RECOVERY} || return 1
	echo "====Start build recovery===="
	make || return 1

	echo -n "--------pack recovery image with zImage and resource...--------"
	RAMDISK_IMAGE=${BS_DIR_BUILDROOT}/output/$BS_CONFIT_RECOVERY/images/rootfs.cpio.gz
	RECOVERY_IMAGE=${BS_DIR_BUILDROOT}/output/$BS_CONFIT_RECOVERY/images/recovery.img
	${BS_DIR_KERNEL}/scripts/mkbootimg --kernel $KERNEL_IMAGE --ramdisk $RAMDISK_IMAGE --second $KERNEL_DTB -o $RECOVERY_IMAGE || return 1
	echo "done."

	cp -v ${BS_DIR_BUILDROOT}/output/$BS_CONFIT_RECOVERY/images/recovery.img ${BS_DIR_OUTPUT}/recovery.img || return 1

	return 0
}

build_update()
{
	cd ${BS_DIR_OUTPUT} || return 1

	source ${BS_DIR_BUILDROOT}/build/envsetup.sh $BS_CONFIT_ROOTFS || return 1
#create oem.img
	# Set oem partition type, including ext2 squashfs
RK_OEM_FS_TYPE=ext2
OEM_DIR=${BS_DIR_TOP}/device/rockchip/oem/oem_normal
OEM_IMG=${BS_DIR_OUTPUT}/oem.img
	${BS_DIR_TOP}/device/rockchip/common/mk-image.sh $OEM_DIR $OEM_IMG $RK_OEM_FS_TYPE || return 1

#create userdata.img
USERDATA_DIR=${BS_DIR_TOP}/device/rockchip/userdata/userdata_normal
USERDATA_IMG=${BS_DIR_OUTPUT}/userdata.img
RK_USERDATA_FS_TYPE=ext2
	${BS_DIR_TOP}/device/rockchip/common/mk-image.sh $USERDATA_DIR $USERDATA_IMG $RK_USERDATA_FS_TYPE || return 1

# copy misc.img
	cp -av ${BS_DIR_TOP}/device/rockchip/rockimg/wipe_all-misc.img ${BS_DIR_OUTPUT}/misc.img || return 1;


	cp -av ${BS_DIR_TOP}/device/rockchip/x3399/parameter-buildroot.txt ${BS_DIR_OUTPUT}/parameter.txt || return 1;
	cp -av ${BS_DIR_TOOLS}/package-file ${BS_DIR_OUTPUT}/package-file || return 1;
#generated update-linux.img
	echo "create update-linux.img..."
	ln -sf ${BS_DIR_OUTPUT}/rootfs-linux.img ${BS_DIR_OUTPUT}/rootfs.img || return 1;
	${BS_DIR_TOP}/tools/linux/Linux_Pack_Firmware/rockdev/afptool -pack ${BS_DIR_OUTPUT}/ ${BS_DIR_OUTPUT}/temp.img || return 1;
	${BS_DIR_TOP}/tools/linux/Linux_Pack_Firmware/rockdev/rkImageMaker -RK330C ${BS_DIR_OUTPUT}/MiniLoaderAll.bin ${BS_DIR_OUTPUT}/temp.img ${BS_DIR_OUTPUT}/update-linux.img -os_type:androidos || return 1;
	rm -rf ${BS_DIR_OUTPUT}/temp.img || return 1;
	rm ${BS_DIR_OUTPUT}/rootfs.img || return 1;
	echo "update-linux.img is generated now!"


	cp -av ${BS_DIR_TOP}/device/rockchip/x3399/parameter-debian.txt ${BS_DIR_OUTPUT}/parameter.txt || return 1;
	cp -av ${BS_DIR_TOOLS}/package-file ${BS_DIR_OUTPUT}/package-file || return 1;
#generated update-debian.img
BS_CONFIG_DEBIAN=rootfs-debian-20190626
if [ -f ${BS_DIR_TOOLS}/rootfs-debian/${BS_CONFIG_DEBIAN}.tar.bz2 ]; then
	echo -e "\033[36m create update-debian.img...... \033[0m"
	if [ ! -f ${BS_DIR_OUTPUT}/${BS_CONFIG_DEBIAN}.img ]; then
		echo -e "\033[36m uncompressing ${BS_CONFIG_DEBIAN} ...... \033[0m"
		tar xjvf ${BS_DIR_TOOLS}/rootfs-debian/${BS_CONFIG_DEBIAN}.tar.bz2 -C ${BS_DIR_OUTPUT}/
	fi
	ln -sf ${BS_DIR_OUTPUT}/${BS_CONFIG_DEBIAN}.img ${BS_DIR_OUTPUT}/rootfs.img || return 1;
	${BS_DIR_TOP}/tools/linux/Linux_Pack_Firmware/rockdev/afptool -pack ${BS_DIR_OUTPUT}/ ${BS_DIR_OUTPUT}/temp.img || return 1;
	${BS_DIR_TOP}/tools/linux/Linux_Pack_Firmware/rockdev/rkImageMaker -RK330C ${BS_DIR_OUTPUT}/MiniLoaderAll.bin ${BS_DIR_OUTPUT}/temp.img ${BS_DIR_OUTPUT}/update-debian.img -os_type:androidos || return 1;
	rm -rf ${BS_DIR_OUTPUT}/temp.img || return 1;
	rm ${BS_DIR_OUTPUT}/rootfs.img || return 1;
	echo -e "\033[36m update-debian.img is generated now! \033[0m"
fi

#generated update-ubuntu.img
BS_CONFIG_UBUNTU=rootfs-ubuntu16-lubuntu-v1.1
if [ -f ${BS_DIR_TOOLS}/rootfs-ubuntu/${BS_CONFIG_UBUNTU}.tar.bz2 ]; then
	echo -e "\033[36m create update-ubuntu.img...... \033[0m"
	if [ ! -f ${BS_DIR_OUTPUT}/${BS_CONFIG_UBUNTU}.img ]; then
		echo -e "\033[36m uncompressing ${BS_CONFIG_UBUNTU} ...... \033[0m"
		tar xvf ${BS_DIR_TOOLS}/rootfs-ubuntu/${BS_CONFIG_UBUNTU}.tar.bz2 -C ${BS_DIR_OUTPUT}/
	fi
	ln -sf ${BS_DIR_OUTPUT}/${BS_CONFIG_UBUNTU}.img ${BS_DIR_OUTPUT}/rootfs.img || return 1;

	${BS_DIR_TOP}/tools/linux/Linux_Pack_Firmware/rockdev/afptool -pack ${BS_DIR_OUTPUT}/ ${BS_DIR_OUTPUT}/temp.img || return 1;
	${BS_DIR_TOP}/tools/linux/Linux_Pack_Firmware/rockdev/rkImageMaker -RK330C ${BS_DIR_OUTPUT}/MiniLoaderAll.bin ${BS_DIR_OUTPUT}/temp.img ${BS_DIR_OUTPUT}/update-ubuntu.img -os_type:androidos || return 1;
	rm -rf ${BS_DIR_OUTPUT}/temp.img || return 1;
	rm ${BS_DIR_OUTPUT}/rootfs.img || return 1;
	echo -e "\033[36m update-ubuntu.img is generated now! \033[0m"
fi

	return 0
}


threads=$(grep processor /proc/cpuinfo | awk '{field=$NF};END{print field+1}')
uboot=no
kernel=no
buildroot=no
recovery=no
update=no

if [ -z $1 ]; then
	uboot=yes
	kernel=yes
	buildroot=yes
	recovery=yes
	update=yes
fi

while [ "$1" ]; do
    case "$1" in
	-j=*)
		x=$1
		threads=${x#-j=}
	    ;;
	-u|--uboot)
		uboot=yes
	    ;;
	-k|--kernel)
		kernel=yes
	    ;;
	-b|--rootfs)
		buildroot=yes
	    ;;
	-r|--recovery)
		recovery=yes
		;;
	-U|--update)
		update=yes
	    ;;
	-a|--all)
		uboot=yes
		kernel=yes
		buildroot=yes
		recovery=yes
		update=yes
	    ;;
	-h|--help)
	    cat >&2 <<EOF
Usage: ./mk.sh [OPTION]
Build script for compile the source of telechips project.

  -j=n                 using n threads when building source project (example: -j=16)
  -u, --uboot          build bootloader uboot from source
  -k, --kernel         build kernel from source
  -b, --rootfs         build linux file system from source
  -r, --recovery       build recovery for linux platform
  -U, --update         build update file
  -a, --all            build all, include anything
  -h, --help           display this help and exit
EOF
	    exit 0
	    ;;
	*)
	    echo "./mk.sh: Unrecognised option $1" >&2
	    exit 1
	    ;;
    esac
    shift
done

setup_environment || exit 1

if [ "${uboot}" = yes ]; then
	build_bootloader_uboot || exit 1
fi

if [ "${kernel}" = yes ]; then
	build_kernel || exit 1
fi

if [ "${buildroot}" = yes ]; then
	build_buildroot || exit 1
fi

if [ "${recovery}" = yes ]; then
	build_recovery || exit 1
fi

if [ "${update}" = yes ]; then
	build_update || exit 1
fi

exit 0
